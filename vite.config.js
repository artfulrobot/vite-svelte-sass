// vite.config.js
import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig({
  base: '', // Do not prepend output paths in index.html with slash
  plugins: [ svelte() ],
  build: {
    outDir: './dist',
    emptyOutDir: true,
    rollupOptions: {
      input: {
        // the key is the output filename without extension.
        // the value is where to find the input.
        'teest': "src/test.scss",
        'other': "src/other.scss",
        'app': "index.html",
      },
      output: {
        // The following ensure we don't have random hashes in output.
        entryFileNames: `[name].js`,
        chunkFileNames: `[name].js`,
        assetFileNames: `[name].[ext]`
      }
    }
  }
})
